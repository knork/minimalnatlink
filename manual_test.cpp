#ifndef UNICODE
#define UNICODE
#endif

#include <windows.h>
#include <cstdio>
#include "MessageThread.h"

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);



LRESULT CALLBACK hiddenWndProc(
        HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    return 0;
}

int main() {
//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
//                   PSTR szCmdLine, int iCmdShow) {


    const wchar_t sample[] = L"Sample Window Class";
    MessageThread thread;
    _sleep(1500);
    thread.displayText(sample, true);
    _sleep(15000);
    getchar();
    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;

        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hwnd, &ps);



            FillRect(hdc, &ps.rcPaint, (HBRUSH) (COLOR_WINDOW+1));

            EndPaint(hwnd, &ps);
        }
            return 0;

    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}