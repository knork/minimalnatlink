/*
 Python Macro Language for Dragon NaturallySpeaking
	(c) Copyright 1999 by Joel Gould
	Portions (c) Copyright 1999 by Dragon Systems, Inc.

 appsupp.h
	The class which NatSpeak calls when it loads a compatibility module.
	This is the definition of a custom COM interface which is called by
	NatSpeak.  The implementation uses ATL for COM intrastructure.
*/

#ifndef appsupp_h
#define appsupp_h

#include "COM/StdAfx.h"
#include "MessageThread.h"

class DECLSPEC_UUID("DD990001-BB89-11d2-B031-0060088DC929") NatLink;
#define IID_IDgnAppSupportA __uuidof(IDgnAppSupportA)

/////////////////////////////////////////////////////////////////////////////
// CDgnAppSupport

class ATL_NO_VTABLE CDgnAppSupport : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CDgnAppSupport, &__uuidof(NatLink)>,
	public IDgnAppSupport
{
public:
	CDgnAppSupport();
	~CDgnAppSupport();

    MessageThread * m_pSecdThrd;

// replaced from resource. #define IDR_APPSUPP                     101
DECLARE_REGISTRY_RESOURCEID(101)
DECLARE_NOT_AGGREGATABLE(CDgnAppSupport)

BEGIN_COM_MAP(CDgnAppSupport)
	COM_INTERFACE_ENTRY(IDgnAppSupport)
END_COM_MAP()

// IDgnAppSupport
public:
	STDMETHOD(UnRegister)();
	STDMETHOD(EndProcess)(DWORD);
	#ifdef UNICODE
		STDMETHOD(AddProcess)(DWORD, const wchar_t *, const wchar_t *, DWORD);
	#else
		STDMETHOD(AddProcess)(DWORD, const char *, const char *, DWORD);
	#endif
	STDMETHOD(Register)(IServiceProvider *);

protected:

};

#endif // appsupp_h
