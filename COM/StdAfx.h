/*
 Python Macro Language for Dragon NaturallySpeaking
	(c) Copyright 1999 by Joel Gould
	Portions (c) Copyright 1999 by Dragon Systems, Inc.

 stdafx.h
	Include file for standard system include files, or project specific
	include files that are used frequently, but are changed infrequently.
*/
#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define UNICODE
#define _UNICODE

// TODO not sure what this guard is for when we already have pragma once
#if !defined(AFX_STDAFX_H__9A6ACE74_B9DB_11D2_B031_0060088DC929__INCLUDED_)
#define AFX_STDAFX_H__9A6ACE74_B9DB_11D2_B031_0060088DC929__INCLUDED_

#define STRICT
#define WINVER 0x0500
#define _WIN32_WINNT 0x0500

//#define _WIN32_WINNT 0x0403
#define _ATL_APARTMENT_THREADED

#include <atlbase.h>
#include <atlcom.h>
#include <comdef.h>
#include <stdio.h>
#include <assert.h>

#include "SPEECH.H"
#include "dspeech.h"
#include "comsupp.h"

#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif

#endif // !defined(AFX_STDAFX_H__9A6ACE74_B9DB_11D2_B031_0060088DC929__INCLUDED)

//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;