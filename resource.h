#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDR_APPSUPP                             101
#define IDC_RICHEDIT                            201
#define IDD_STDOUT                              201
#define IDR_MENU                                202
#define IDD_WAITFOR                             203
#define IDD_RELOAD                              32768
#define IDS_PROJNAME                            40000
