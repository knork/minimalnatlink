#include <iostream>
#include "COM/StdAfx.h"
#include "appsupp.h"

//CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(__uuidof(NatLink), CDgnAppSupport)
END_OBJECT_MAP()

/////////////////////////////////////////////////////////////////////////////
// DLL Entry Point

extern "C"
BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID /*lpReserved*/)
{
if (dwReason == DLL_PROCESS_ATTACH)
{
_Module.Init(ObjectMap, hInstance);
DisableThreadLibraryCalls(hInstance);

// this is needed for when you run this module from within Python
// and you use the second thread with its dialog box
LoadLibrary( TEXT( "riched32" ) ); // RW TEXT macro added
}
else if (dwReason == DLL_PROCESS_DETACH)
_Module.Term();
    std::cout << "DLL main" << std::endl;
return TRUE;    // ok
}

/**
 * Required by the COM-server definition
 * https://de.wikipedia.org/wiki/Component_Object_Model#In-process-Server
 * Used to determine whether the DLL can be unloaded by OLE
 */
STDAPI DllCanUnloadNow(void)
{
    return (_Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/**
 * Required by the COM-server definition
 * https://de.wikipedia.org/wiki/Component_Object_Model#In-process-Server
 *  Returns a class factory to create an object of the requested type
 */
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/**
 * Required by the COM-server definition
 * https://de.wikipedia.org/wiki/Component_Object_Model#In-process-Server
 * DllRegisterServer - Adds entries to the system registry
 * This should be called during the installation process
 * and provide natspeak with the path to our pyd/dll
 */
STDAPI DllRegisterServer(void)
{
    // registers object (see appsupp.reg)
//	todo why this is set to false?
    return _Module.RegisterServer(FALSE);
}

/**
 * Required by the COM-server definition
 * https://de.wikipedia.org/wiki/Component_Object_Model#In-process-Server
 * DllUnregisterServer - Removes entries from the system registry
 */
STDAPI DllUnregisterServer(void)
{
    _Module.UnregisterServer();
    return S_OK;
}


