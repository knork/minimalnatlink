/*
 Python Macro Language for Dragon NaturallySpeaking
	(c) Copyright 1999 by Joel Gould
	Portions (c) Copyright 1999 by Dragon Systems, Inc.

 secdthrd.cpp
	This file contains the code which handles the second thread used in the
	natlink module.  The second thread provised a dialog in which we display
	Python error messages

 April 25, 1999
	- packaged for external release

 March 3, 1999
	- initial version
*/

#include "COM/StdAfx.h"
#include "resource.h"
#include "MessageThread.h"
#include <Richedit.h>
#include <iostream>
#include <windows.h>
#include <strsafe.h>



// This is the message we send to the other thread to add text to the output
// window.  wParam = TRUE for error text, FALSE for normal text.  lParam is
// the pointer to a string to display.  The thread should delete the string
// when finished.
#define WM_SHOWTEXT (WM_USER+362)

// Send this message to the other thread to destroy the window and exit the
// thread.

#define WM_EXITTHREAD (WM_USER+363)

#define DARKRED RGB( 128, 0, 0 )


//---------------------------------------------------------------------------
// Called when a message is sent to the dialog box.  Return FALSE to tell
// the dialog box default window message handler to process the message.
// I made this struct to more conveniently store the positions / size of each window in the dialog
typedef struct SizeAndPos_s
{
    int x, y, width, height;
} SizeAndPos_t;

HWND txtEditHandle = NULL;

const WORD ID_txtEdit = 4;
INT_PTR CALLBACK
dialogProc(
        HWND
        hWnd,
        UINT msg, WPARAM
        wParam,
        LPARAM lParam
) {
// This is the threads copy of the MessageThread class pointer. I
// supposed that this should not really be global but stored in some
// thread or windows storage instead. Oh, well.
    static MessageThread *s_pSecdThrd = NULL;

    switch (msg) {

        case WM_INITDIALOG:
            std::cout << "Message initialization" <<
                      std::endl;
            s_pSecdThrd = (MessageThread *) lParam;
            ShowWindow(hWnd, SW_HIDE
            );
            return
                    TRUE;

        case WM_EXITTHREAD:
            DestroyWindow(hWnd);
            return
                    TRUE;

        case WM_DESTROY:
            PostQuitMessage(0);
            return
                    FALSE;

        case WM_CLOSE:
// do not really close, hide instead
            ShowWindow(hWnd, SW_HIDE
            );
// also clear out the contents of the window
            SetDlgItemText(hWnd,
                           IDC_RICHEDIT, TEXT("")); // RW TEXT macro added
            return
                    TRUE;

        case WM_COMMAND:
            if (
                    s_pSecdThrd && s_pSecdThrd
                            ->

                                    getMsgWnd()

                    ) {
                PostMessage(s_pSecdThrd
                                    ->

                                            getMsgWnd(), msg, wParam, lParam

                );
            }
            return
                    TRUE;

        case WM_SIZE: // RW Added: track resizing of the window and call repaint
        {
            HWND hEdit = GetDlgItem(hWnd, IDC_RICHEDIT);
            MoveWindow(hEdit,
                       0, 0,
                       LOWORD(lParam),        // width of client area
                       HIWORD(lParam),        // height of client area
                       TRUE
            );                    // repaint window
        }
            return
                    TRUE;

        case WM_SHOWTEXT: {
            char *pszText = (char *) lParam;

            ShowWindow(hWnd, SW_SHOW
            );

            CHARFORMAT chForm;
            chForm.
                    cbSize = sizeof(chForm);
            chForm.
                    dwMask = CFM_COLOR;
            chForm.
                    crTextColor =
                    wParam ? DARKRED : GetSysColor(COLOR_WINDOWTEXT);
            chForm.
                    dwEffects = 0;

            HWND hEdit = GetDlgItem(hWnd, IDC_RICHEDIT);
            int ndx = GetWindowTextLength(hEdit);
#ifdef WIN32
            SendMessage(hEdit, EM_SETSEL, (WPARAM)
                    ndx, (LPARAM) ndx);
#else
            SendMessage( hEdit, EM_SETSEL, 0, MAKELONG( ndx, ndx ) );
#endif
//SendMessage( hEdit, EM_SETSEL, 0x7FFF, 0x7FFF );
            SendMessage(hEdit, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)
                    &chForm);
#ifdef UNICODE
            int size_needed = ::MultiByteToWideChar(CP_ACP, 0, pszText, -1, NULL, 0);
            TCHAR *pszTextW = new TCHAR[size_needed];
            ::MultiByteToWideChar(CP_ACP,
                                  0, pszText, -1, pszTextW, size_needed);

            SendMessage(hEdit, EM_REPLACESEL, FALSE, (LPARAM)
                    pszTextW);
            if (pszTextW)
                delete[]
                        pszTextW;
#else
                SendMessage( hEdit, EM_REPLACESEL, FALSE, (LPARAM)pszText );
#endif
//SendMessage( hEdit, EM_SETSEL, 0x7FFF, 0x7FFF );
            SendMessage(hEdit, EM_SCROLLCARET,
                        0, 0);
            if (pszText)
                delete
                        pszText;
        }
            return
                    TRUE;

        default:
            return
                    FALSE;
    }
}

//---------------------------------------------------------------------------
// This is the main routine which the thread runs.  It contains a windows
// message loop and will not return until the thread returns.
HMODULE GetCurrentModuleHandle() {
    HMODULE hModule = NULL;
    GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS,
                      (LPCTSTR) GetCurrentModuleHandle,
                      &hModule);

    return hModule;
}

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM
);

BOOL CALLBACK AboutDlgProc(HWND, UINT, WPARAM, LPARAM
);

DWORD CALLBACK threadMain(void *pArg) {
    MessageThread *pThis = (MessageThread *) pArg;

    // create a dialog box to display the messages
    HINSTANCE hInstance = GetCurrentModuleHandle();
    WNDCLASS wndclass;
    // Register the window class.
    const wchar_t CLASS_NAME[] = L"Sample Window Class";

    WNDCLASS wc = {};
    wc.lpfnWndProc = WndProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = CLASS_NAME;
    RegisterClass(&wc);

    // Create the window.
    HWND hwnd = CreateWindowEx(
            0,                              // Optional window styles.
            CLASS_NAME,                     // Window class
            L"Natlink 2.0",    // Window text
            WS_OVERLAPPEDWINDOW,            // Window style

            // Size and position
            10, 10, 300, 500,
            NULL,       // Parent window
            NULL,       // Menu
            hInstance,  // Instance handle
            NULL        // Additional application data
    );

    if (hwnd == NULL) {
        return 0;
    }

    pThis->signalEvent();

    ShowWindow(hwnd, SW_SHOW);

    // Run the message loop.

    MSG msg = {};
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}


LRESULT CALLBACK
WndProc(HWND
        hwnd,
        UINT message, WPARAM
        wParam,
        LPARAM lParam
) {
    static HINSTANCE hInstance;

    switch (message) {
        case WM_CREATE :
            hInstance = ((LPCREATESTRUCT) lParam)->hInstance;

            LoadLibrary(L"riched32.dll");
            txtEditHandle   = CreateWindowEx(0, L"RichEdit", TEXT("Type here \n more text \neven more text \ntext\neven more text \ntext\neven more text \ntext\neven more text \ntext"),
                                             ES_MULTILINE | WS_VISIBLE | WS_CHILD | WS_BORDER | WS_TABSTOP | ES_READONLY| ES_AUTOVSCROLL | ES_AUTOHSCROLL | WS_VSCROLL | WS_HSCROLL,
                                             10,10,150,100,
                                             hwnd,           (HMENU)ID_txtEdit,  NULL,           NULL);
            return 0;


        case WM_DESTROY :
            PostQuitMessage(0);
            return 0;
        case WM_SIZE:
            // Make the edit control the size of the window's client area.

            MoveWindow(txtEditHandle,
                       0, 0,                  // starting x- and y-coordinates
                       LOWORD(lParam),        // width of client area
                       HIWORD(lParam),        // height of client area
                       TRUE);                 // repaint window
            return 0;
    }
    return
            DefWindowProc(hwnd, message, wParam, lParam
            );
}

//---------------------------------------------------------------------------

MessageThread::MessageThread() {


    m_hEvent = CreateEvent(
            NULL,    // security options
            TRUE,    // manual reset
            FALSE,    // not signaled
            NULL);    // name
    assert(m_hEvent != NULL);

    DWORD dwId;
    threadHandle = CreateThread(
            NULL,            // security (use default)
            0,    // stack size (use default)
            threadMain,        // thread function
            (void *) this,    // argument that is passed to the thread
            0,// creation flags - 0 The thread runs immediately
            &dwId);        // address for returned thread ID
    assert(threadHandle != NULL);

    //	somehow this tells us when the thread has finished setting up
    if (m_hEvent) {
        WaitForSingleObject(m_hEvent, 1000);
        CloseHandle(m_hEvent);
        m_hEvent = NULL;
    }
}

//---------------------------------------------------------------------------

MessageThread::~MessageThread() {
    // terminate the output window then wait for the thread to terminate

    if (m_hOutWnd) {
        PostMessage(m_hOutWnd, WM_EXITTHREAD, 0, 0);
        m_hOutWnd = NULL;
    }
    if (threadHandle != NULL) {
        WaitForSingleObject(threadHandle, 1000 /*ms*/ );
        threadHandle = NULL;
    }
}

//---------------------------------------------------------------------------

void MessageThread::displayText(const char *pszText, BOOL bError) {
    if (txtEditHandle) {


        int tl;
        CHARFORMAT cf;
        tl = GetWindowTextLength(txtEditHandle);

        // add new text
        SendMessage(txtEditHandle, EM_SETSEL, tl, tl);
        SendMessage(txtEditHandle, EM_REPLACESEL, FALSE, (LPARAM)pszText);

        // fill struct
        memset( &cf, 0, sizeof(CHARFORMAT) );

        cf.cbSize = sizeof(CHARFORMAT);
        cf.dwMask = CFM_COLOR;
        // The color for error messages
        if(bError)
            cf.crTextColor = RGB( 128, 0, 0 );
        else
            cf.crTextColor = RGB( 0, 0, 0 );
        // select last text inserted
        tl = GetWindowTextLength(txtEditHandle);
        SendMessage(txtEditHandle, EM_SETSEL, (WPARAM)(tl-strlen(pszText)), (LPARAM)tl);

        // set color
        SendMessage(txtEditHandle, EM_SETCHARFORMAT ,SCF_SELECTION, (LPARAM)&cf);

        // remove bracket
        tl = GetWindowTextLength(txtEditHandle);
        SendMessage(txtEditHandle, EM_SETSEL, tl, tl);

        // set cusor to end of text
        for(int l=0; l<SendMessage(txtEditHandle, EM_GETLINECOUNT, 0, 0); l++)
            SendMessage(txtEditHandle, EM_SCROLL, SB_LINEDOWN, 0);

    }
}
